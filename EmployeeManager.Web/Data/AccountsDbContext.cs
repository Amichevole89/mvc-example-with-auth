﻿// Data/AccountsDbContextFactory.cs
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Session;
using EmployeeManager.Web.Models;

namespace EmployeeManager.Web.Data;

public class AccountsDbContextFactory(DbContextOptions<AccountsDbContextFactory> options)
    : IdentityDbContext<UserAccount, IdentityRole<Guid>, Guid>(options)
{
    public DbSet<UserAccount> UserAccounts { get; init; }
    public DbSet<Campaign> Campaigns { get; set; }
    public DbSet<Contact> Contacts { get; set; }
    public DbSet<ContactType> ContactTypes { get; set; }
    public DbSet<Establishment> Establishments { get; set; }
    public DbSet<Location> Locations { get; set; }
    public DbSet<CampaignLocation> CampaignLocations { get; set; }
    public DbSet<CampaignLocationEvent> CampaignLocationEvents { get; set; }
    public DbSet<People> People { get; set; }




}