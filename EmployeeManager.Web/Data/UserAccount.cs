using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace EmployeeManager.Web.Data;

public class UserAccount : IdentityUser<Guid>
{
    [Required]
    [ProtectedPersonalData]
    [StringLength(200,
        MinimumLength = 1,
        ErrorMessage = "First name must be between {1} and {2} characters")]
    public string FirstName { get; set; } = "";

    [Required]
    [ProtectedPersonalData]
    [StringLength(50,
        MinimumLength = 1,
        ErrorMessage = "Last name must be between {1} and {2} characters")]
    public string LastName { get; set; } = "";

}

