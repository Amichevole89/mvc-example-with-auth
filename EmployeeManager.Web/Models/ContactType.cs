using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeManager.Web.Models
{
    [Table("ContactTypes", Schema = "public")]
    public class ContactType: EntityBase
    {
        [Required]
        [StringLength(50,
        MinimumLength = 1,
        ErrorMessage = "Name must be between {1} and {50} characters")]
        public string Name { get; set; } = "";

        public List<Contact> Contacts { get; set; } = [];

    }
}
