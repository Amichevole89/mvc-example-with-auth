using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeManager.Web.Models
{
    [Table("CampaignLocations", Schema = "public")]
    public class CampaignLocation: EntityBase
    {

        [Required]
        public int LocationId { get; set; }

        [ForeignKey("LocationId")]
        public Location? Location { get; set; }

        [Required]
        public int CampaignId { get; set; }

        [ForeignKey("CampaignId")]
        public Campaign? Campaign { get; set; }

        [DataType(DataType.MultilineText)]
        public string Notes { get; set; } = "";

        public List<CampaignLocationEvent> CampaignLocationEvents { get; set; } = [];
    }

}