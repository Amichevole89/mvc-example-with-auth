using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeManager.Web.Models
{
    [Table("Contacts", Schema = "public")]
    public class Contact: EntityBase
    {

        [Required]
        public int ContactTypeId { get; set; }

        [ForeignKey("ContactTypeId")]
        public ContactType? ContactType { get; set; }

        [Required]
        public int EstablishmentId { get; set; }

        [ForeignKey("EstablishmentId")]
        public Establishment? Establishment { get; set; }

        [Required]
        public int PeopleId { get; set; }

        [ForeignKey("PeopleId")]
        public required People People { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [DisplayFormat(DataFormatString = "{0:(###) ###-####}", ApplyFormatInEditMode = true)]
        [StringLength(20,
            MinimumLength = 10,
            ErrorMessage = "Phone must be between {10} and {20} characters")]
        public string Phone { get; set; } = "";

        [Required]
        public bool Active { get; set; } = true;


    }
}
