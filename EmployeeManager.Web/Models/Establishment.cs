using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeManager.Web.Models
{
    [Table("Establishments", Schema = "public")]
    public class Establishment: EntityBase
    {

        [Required]
        [StringLength(200,
            MinimumLength = 1,
            ErrorMessage = "Name must be between {1} and {2} characters")]
        public string Name { get; set; } = "";

        public List<Location> Locations { get; set; } = [];
        public List<Contact> Contacts { get; set; } = [];
    }
}