using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeManager.Web.Models
{
    [Table("CampaignLocationEvents", Schema = "public")]
    public class CampaignLocationEvent: EntityBase
    {

        [Required]
        public int CampaignLocationId { get; set; }

        [ForeignKey("CampaignLocationId")]
        public CampaignLocation? CampaignLocations { get; set; }

        [Required]
        public int EventTypeId { get; set; }

        [ForeignKey("EventTypeId")]
        public EventType? EventType { get; set; }

        [Required]
        public int ParticipantCount { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Entered { get; set; } = DateTime.UtcNow;

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Invoice { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Paid { get; set; }

        [DataType(DataType.MultilineText)]
        public string? Notes { get; set; }
    }
}
