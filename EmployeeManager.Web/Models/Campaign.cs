using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeManager.Web.Models
{
    [Table("Campaigns", Schema = "public")]
    public class Campaign : EntityBase
    {
        [Required(ErrorMessage = "Name is required")]
        [StringLength(200,
           MinimumLength = 1,
           ErrorMessage = "Name must be between {1} and {2} characters")]
        public required string Name { get; set; }

        [Required(ErrorMessage = "Start Date is required")]
        [DataType(DataType.Date)]
        public DateOnly StartDate { get; set; }

        [Required(ErrorMessage = "End Date is required")]
        [DataType(DataType.Date)]
        public DateOnly EndDate { get; set; }

        public List<EventType>? EventTypes { get; set; }
        public List<CampaignLocation> CampaignLocations { get; set; } = [];
    }
}