using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EmployeeManager.Web.Data;
using Microsoft.EntityFrameworkCore.Storage;

namespace EmployeeManager.Web.Models
{
  public class People : EntityBase
  {

    [Required]
    [StringLength(200,
        MinimumLength = 1,
        ErrorMessage = "Name must be between {1} and {200} characters")]
    public string Name { get; set; } = "";

    [Required]
    [DataType(DataType.EmailAddress)]
    [StringLength(254,
        MinimumLength = 1,
        ErrorMessage = "Email must be between {1} and {254} characters")]
    public string Email { get; set; } = "";

    public List<UserAccount> UserAccounts { get; set; } = [];

    public List<Contact> Contacts { get; set; } = [];


  }
}
