// Data/Session.cs
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EmployeeManager.Web.Data;

public class Session
{
    public int Id { get; set; }
    
    [Required]
    public Guid AccountId { get; set; }

    [ForeignKey("AccountId")]
    public UserAccount? Account { get; set; }

    public Guid Jti { get; set; }

    public Guid RefreshToken { get; set; }

    public bool Active { get; set; }

    public DateTime Created { get; set; }

}