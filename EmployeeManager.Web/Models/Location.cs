using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeManager.Web.Models
{
    [Table("Locations", Schema = "public")]
    public class Location: EntityBase
    {
        [Required]
        public int EstablishmentId { get; set; }

        [ForeignKey("EstablishmentId")]
        public Establishment? Establishment { get; set; }

        [Required]
        [StringLength(100,
            MinimumLength = 1,
            ErrorMessage = "Name must be between {1} and {100} characters")]
        public string Name { get; set; } = "";

        [Required]
        [StringLength(500,
            MinimumLength = 1,
            ErrorMessage = "Address must be between {1} and {500} characters")]
        public string Address { get; set; } = "";

        [Required]
        [StringLength(30,
            MinimumLength = 1,
            ErrorMessage = "City must be between {1} and {30} characters")]
        public string City { get; set; } = "";

        [Required]
        [StringLength(2,
            MinimumLength = 2,
            ErrorMessage = "State must be 2 characters")]
        public string AddressState { get; set; } = "";

        [Required]
        [DataType(DataType.PostalCode)]
        [StringLength(10,
            MinimumLength = 5,
            ErrorMessage = "Postal Code must be between {1} and {10} characters")]
        public string PostalCode { get; set; } = "";

        public List<CampaignLocation> CampaignLocations { get; set; } = [];
    }

}
