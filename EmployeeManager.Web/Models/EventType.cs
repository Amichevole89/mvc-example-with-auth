using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeManager.Web.Models
{
    [Table("EventTypes", Schema = "public")]
    public class EventType: EntityBase
    {

        [Required]
        public int CampaignId { get; set; }

        [ForeignKey("CampaignId")]
        public Campaign? Campaign { get; set; }

        [Required]
        [StringLength(100,
            MinimumLength = 1,
            ErrorMessage = "Name must be between {1} and {100} characters")]
        public string Name { get; set; } = "";

        [Required]
        [DataType(DataType.Currency)]
        public decimal Cost { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal Donation { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal MinimumDonation { get; set; }


        public List<CampaignLocationEvent> CampaignLocationEvents { get; set; } = [];
    }
}

