using EmployeeManager.Web.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeeManager.Web.Models;

namespace EmployeeManager.Web.Controllers
{
    public class EstablishmentsController(IDbContextFactory<AccountsDbContextFactory> dbContextFactory) : Controller
    {
        private readonly IDbContextFactory<AccountsDbContextFactory> _dbContextFactory = dbContextFactory;

        // GET: /Establishments/
        public async Task<IActionResult> Index()
        {
            using var context = _dbContextFactory.CreateDbContext();
            var establishments = await context.Establishments.ToListAsync();
            return View(establishments);
        }
        // GET: /Establishments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();

            using var context = _dbContextFactory.CreateDbContext();
            var establishment = await context.Establishments.FirstOrDefaultAsync(m => m.Id == id);

            if (establishment == null) return NotFound();

            return View(establishment);
        }

        // GET: /Establishments/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: /Establishments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Establishment establishment)
        {
            if (ModelState.IsValid)
            {
                using var context = _dbContextFactory.CreateDbContext();
                context.Add(establishment);
                await context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(establishment);
        }

        // GET: /Establishments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();

            using var context = _dbContextFactory.CreateDbContext();
            var establishment = await context.Establishments.FirstOrDefaultAsync(m => m.Id == id);

            if (establishment == null) return NotFound();

            return View(establishment);
        }

        // POST: /Establishments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Establishment establishment)
        {
            if (id != establishment.Id) return NotFound();

            if (ModelState.IsValid)
            {
                using var context = _dbContextFactory.CreateDbContext();
                try
                {
                    context.Update(establishment);
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EstablishmentExists(establishment.Id)) return NotFound();
                    else throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(establishment);
        }

        // GET: /Establishments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();
            using var context = _dbContextFactory.CreateDbContext();
            var establishment = await context.Establishments.FirstOrDefaultAsync(m => m.Id == id);
            if (establishment == null) return NotFound();
            return View(establishment);
        }

        // POST: /Establishments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            using var context = _dbContextFactory.CreateDbContext();
            var establishment = await context.Establishments.FindAsync(id);
            if (establishment == null) return NotFound();
            context.Establishments.Remove(establishment);
            await context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EstablishmentExists(int id)
        {
            using var context = _dbContextFactory.CreateDbContext();

            return context.Establishments.Any(e => e.Id == id);
        }


    }
}