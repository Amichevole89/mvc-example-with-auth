// using System.Linq;
// using Microsoft.AspNetCore.Mvc;
// using EmployeeManager.Web.Models;
// using Microsoft.EntityFrameworkCore;
// using Microsoft.Extensions.Logging;
// using System.Threading.Tasks;
// using EmployeeManager.Web.Data;

// namespace EmployeeManager.Web.Controllers
// {
//     public class CampaignController : Controller
//     {
//         private readonly ILogger<CampaignController> _logger;
//         private readonly IDbContextFactory<AccountsDbContextFactory> _dbContextFactory;

//         public CampaignController(ILogger<CampaignController> logger, IDbContextFactory<AccountsDbContextFactory> dbContextFactory)
//         {
//             _logger = logger;
//             _dbContextFactory = dbContextFactory;
//         }

//         public IActionResult Index()
//         {
//             return View();
//         }

//         [HttpGet]
//         public async Task<IActionResult> Search(string query)
//         {
//             using var dbContext = _dbContextFactory.CreateDbContext();

//             // Perform search in the database for campaigns whose names or descriptions contain the query
//             var searchResults = await dbContext.Campaigns
//                 .Where(c => c.Name.Contains(query) || c.Name.Contains(query))
//                 .ToListAsync();

//             return PartialView("_SearchResultsPartial", searchResults);
//         }
//     }
// }
