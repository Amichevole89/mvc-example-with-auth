using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using EmployeeManager.Web.Models;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using EmployeeManager.Web.Data;

namespace EmployeeManager.Web.Controllers
{
    public class HomeController(ILogger<HomeController> logger, IDbContextFactory<AccountsDbContextFactory> dbContextFactory) : Controller
    {
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var context = dbContextFactory.CreateDbContext();
            var idClaim = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
            if (idClaim == null || !Guid.TryParse(idClaim.Value, out var userId)) return View();
            var account = await context.Users.FirstOrDefaultAsync(x => x.Id == userId);
            return View(account);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}