using Microsoft.AspNetCore.Mvc;
using EmployeeManager.Web.Models;
using Microsoft.EntityFrameworkCore;
using EmployeeManager.Web.Data;

namespace EmployeeManager.Web.Controllers
{
    public class LocationsController(IDbContextFactory<AccountsDbContextFactory> dbContextFactory) : Controller
    {
        private readonly IDbContextFactory<AccountsDbContextFactory> _dbContextFactory = dbContextFactory;

        // GET: /Locations/
        public async Task<IActionResult> Index()
        {
            using var context = _dbContextFactory.CreateDbContext();
            var locations = await context.Locations.ToListAsync();
            return View(locations);
        }
        // GET: /Locations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null) return NotFound();


            using var context = _dbContextFactory.CreateDbContext();
            var location = await context.Locations.FirstOrDefaultAsync(m => m.Id == id);

            if (location == null) return NotFound();


            return View(location);
        }

        // GET: /Locations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: /Locations/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Location location)
        {
            if (ModelState.IsValid)
            {
                using var context = _dbContextFactory.CreateDbContext();
                context.Add(location);
                await context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(location);
        }

        // GET: /Locations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();


            using var context = _dbContextFactory.CreateDbContext();
            var location = await context.Locations.FindAsync(id);
            if (location == null) return NotFound();

            return View(location);
        }

        // POST: /Locations/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Location location)
        {
            if (id != location.Id) return NotFound();


            if (ModelState.IsValid)
            {
                using var context = _dbContextFactory.CreateDbContext();
                try
                {
                    context.Update(location);
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LocationExists(location.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(location);
        }

        // GET: /Locations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null) return NotFound();

            using var context = _dbContextFactory.CreateDbContext();
            var location = await context.Locations.FirstOrDefaultAsync(m => m.Id == id);
            if (location == null) return NotFound();


            return View(location);
        }

        // POST: /Locations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            using var context = _dbContextFactory.CreateDbContext();
            var location = await context.Locations.FindAsync(id);
            if (location == null) return NotFound();
            context.Locations.Remove(location);
            await context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LocationExists(int id)
        {
            using var context = _dbContextFactory.CreateDbContext();
            return context.Locations.Any(e => e.Id == id);
        }

    }
}