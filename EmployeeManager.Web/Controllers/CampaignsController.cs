using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

using EmployeeManager.Web.Models;
using Microsoft.EntityFrameworkCore;
using EmployeeManager.Web.Data;

namespace EmployeeManager.Web.Controllers
{
    
    public class CampaignsController(IDbContextFactory<AccountsDbContextFactory> dbContextFactory) : Controller
    {
        private readonly IDbContextFactory<AccountsDbContextFactory> _dbContextFactory = dbContextFactory;

        // GET: /Campaigns/
        public async Task<IActionResult> Index()
        {
            using var context = _dbContextFactory.CreateDbContext();
            var campaigns = await context.Campaigns.ToListAsync();
            return View(campaigns);
        }
        // GET: /Campaigns/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            using var context = _dbContextFactory.CreateDbContext();
            var campaign = await context.Campaigns.FirstOrDefaultAsync(m => m.Id == id);

            if (campaign == null)
            {
                return NotFound();
            }

            return View(campaign);
        }

        // GET: /Campaigns/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: /Campaigns/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Campaign campaign)
        {
            if (ModelState.IsValid)
            {
                using var context = _dbContextFactory.CreateDbContext();
                context.Add(campaign);
                await context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(campaign);
        }

        // GET: /Campaigns/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            using var context = _dbContextFactory.CreateDbContext();
            var campaign = await context.Campaigns.FindAsync(id);

            if (campaign == null)
            {
                return NotFound();
            }

            return View(campaign);
        }

        // POST: /Campaigns/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Campaign campaign)
        {
            if (id != campaign.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using var context = _dbContextFactory.CreateDbContext();
                    context.Update(campaign);
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CampaignExists(campaign.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(campaign);
        }

        // GET: /Campaigns/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            using var context = _dbContextFactory.CreateDbContext();
            var campaign = await context.Campaigns.FirstOrDefaultAsync(m => m.Id == id);

            if (campaign == null) return NotFound();

            return View(campaign);
        }

        // POST: /Campaigns/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            using var context = _dbContextFactory.CreateDbContext();
            var campaign = await context.Campaigns.FindAsync(id);
            if (campaign == null) return NotFound();
            context.Campaigns.Remove(campaign);
            await context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CampaignExists(int id)
        {
            using var context = _dbContextFactory.CreateDbContext();
            return context.Campaigns.Any(e => e.Id == id);
        }
    }
}
